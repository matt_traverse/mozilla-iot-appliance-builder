# Customized distribution images for Traverse Layerscape boards

This is a collection of distribution images for Traverse Layerscape boards that have
certain customizations built in, including but not limited to:

- Using the [Traverse Kernel Packages](https://gitlab.com/traversetech/traverse-kernel-patches)
  if the distributions default kernel does not ship with DPAA2/Layerscape drivers

- Installation of the [Traverse Board Support](https://gitlab.com/traversetech/ls1088firmware/traverse-board-support)
  package to handle any required user space set up

- Setting a one time root password for access without cloud-init

The building of images is done with the OpenStack [diskimage-builder](https://github.com/openstack/diskimage-builder).

At the moment only Debian images are being built - this will be extended soon.

All images are tested as both a virtual machine and on bare metal for quality assurance.

## Download and usage
You can download images from our [archive server](https://archive.traverse.com.au/pub/traverse/ls1088firmware/distribution-images/).

The best way to install them on a Ten64 board is via the [recovery environment](https://ten64doc.traverse.com.au/software/recovery/) feature.

Images built here have a default root password of `changethis`. You will be asked to change the password on the first login.
