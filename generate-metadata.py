#!/usr/bin/env python3

import os
import sys
import json
from json import JSONEncoder

class ImageRegistryEntry:
    idstr = None
    name = None
    description = None
    maintainer = None
    download = None
    link = None
    sha256sum = None
    format = None
    version = None
    mindisk = None

    def __init__(self, newid):
        self.idstr = newid

class ImageRegistryEntryEncoder(JSONEncoder):
    def default(self, obj):
        if(isinstance(obj, ImageRegistryEntry)):
            selfdict = obj.__dict__
            selfdict["id"] = obj.idstr
            del selfdict["idstr"]
            return selfdict
        return JSONEncoder.default(self, obj)

def read_image_sha256(checksumfile):
    computed_sha256file = open(checksumfile, 'r')
    computed_sha256data = computed_sha256file.read()
    sha256sum = computed_sha256data.split(" ")[0].strip()
    computed_sha256file.close()
    return sha256sum

def output_dictionary(outputfilename, entry):
    outputfile = open(outputfilename, 'w')
    json.dump(entry, outputfile, cls=ImageRegistryEntryEncoder)
    outputfile.close()

if __name__ == "__main__":
    arguments = sys.argv
    idstr = arguments[1]
    imagefile = arguments[2]
    name = arguments[3]
    description = arguments[4]
    version = arguments[5]
    downloadfile = arguments[6]
    checksumfile = arguments[7]
    outputfile = arguments[8]

    entry = ImageRegistryEntry(idstr)
    entry.name = name
    entry.description = description
    entry.version = version
    entry.format = "qcow2"
    entry.download = downloadfile
    if ("CI_PROJECT_URL" in os.environ):
        entry.link = os.environ["CI_PROJECT_URL"]

    sha256sum = read_image_sha256(checksumfile)
    entry.sha256sum = sha256sum

    output_dictionary(outputfile, entry)
