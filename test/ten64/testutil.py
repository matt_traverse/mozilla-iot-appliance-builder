import pexpect.fdpexpect
import os
import telnetlib
import requests
import re
import time
import sys

import paramiko.client
from contextlib import suppress

from paramiko import SSHClient
from scp import SCPClient

# 1%
percent_free_expression = re.compile("(\d+)%")
number_match = re.compile("(\\d)")
ipv4_expression = re.compile("(\d+\.\d+\.\d+\.\d+)")

ssh_firewall_commands = [
    'uci set firewall.allowssh=rule',
    "uci set firewall.allowssh.src='wan'",
    "uci set firewall.allowssh.proto='tcp'",
    "uci set firewall.allowssh.dest_port='22'",
    "uci set firewall.allowssh.target='ACCEPT'"
]


class BareMetalTestClient:
    recovery_ps1 = "testboard:/tmp\$ "
    remote_block_device = None
    console_host = None
    console_port = 23
    redfishbase = None
    wan_intf = None
    wan_intf_addr = None
    target_ps1 = None
    target_login = None
    expected_ethernet_intf = 10

    def __init__(self, telnethost, telnetport, redfish_url, remote_block, wan_interface):
        self.console_host = telnethost
        self.console_port = telnetport
        self.redfishbase = redfish_url
        self.remote_block_device = remote_block
        self.wan_intf = wan_interface

    def connect(self):
        self.telsh = telnetlib.Telnet(self.console_host,self.console_port)
        self.client = pexpect.fdpexpect.fdspawn(self.telsh,encoding='us-ascii',codec_errors='ignore')

    def reset_remote_system(self):
        print("Resetting remote system")
        # Reset the target into u-boot first
        r = requests.post('{}/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/'.format(self.redfishbase)
            ,json={'ResetType':'ForceRestart'})
        r.raise_for_status()

    def reset_into_default_boot(self):
        self.reset_remote_system()
        self.client.expect('=>')
        self.client.sendline(u'run setup_distroboot_efi && run distro_bootcmd')

    def send_simple_command(self, command, ps1=recovery_ps1, timeout=10):
        self.client.sendline(command)
        self.client.expect(ps1,timeout=timeout)

    def do_returncode_check(self, ps1=recovery_ps1):
        self.client.sendline("echo $?")
        self.client.expect('0')
        self.client.expect(ps1)

    def make_ready_for_download(self):
        self.reset_remote_system()
        self.client.expect('=>')
        self.client.sendline(u'run bootcmd_recovery')
        self.client.expect(u'Please press Enter to activate this console.',timeout=120)
        # Wait for modules to be loaded and dmesg to clear up
        time.sleep(10)
        self.client.sendline(u'')
        #Reset PS1 (it could be set to anything)
        self.client.sendline(u'export PS1="testboard:\w\$ "')
        self.send_simple_command(u'cd /tmp')

        # Wait for the network stack to come up (this can happen after the console is ready)
        # otherwise we encounter issues later trying to shut off the firewall
        for i in range(1,5):
            self.client.sendline("ubus -v call network.interface.lan \"status\" | jsonfilter -e '@.up'")
            status = self.client.expect(["true","false"])
            self.client.expect(self.recovery_ps1)
            if (status == 0):
                break
        time.sleep(5)

        # Wait for WAN to come up
        self.send_simple_command(u'ifdown lan')
        self.send_simple_command('set-wan {}'.format(self.wan_intf))
        # If network UCI package is not committed, firewall rules may not be consistent
        self.send_simple_command('uci commit network')
        for i in range(1,5):
            self.client.sendline("ubus -v call network.interface.wan \"status\" | jsonfilter -e '@.up'")
            status = self.client.expect(["true","false"])
            self.client.expect(self.recovery_ps1)
            if (status == 0):
                break
            # Some of our test systems are connected via switches with STP
            # - give time for switch to enable traffic
            time.sleep(10)

        self.client.sendline("ubus -v call network.interface.wan \"status\" | jsonfilter -e '@[\"ipv4-address\"][0].address'")
        self.client.expect(ipv4_expression)

        ipv4_regex_match = self.client.match
        self.wan_intf_addr = ipv4_regex_match.group(1)


    def do_ssh_transfer(self,local_image_path,remote_image_path, with_sha256sum=True):
        ssh = SSHClient()
        # The recovery environment is an initramfs which doesn't persist its ssh key,
        # hence allowing missing host keys is intentional
        ssh.set_missing_host_key_policy(paramiko.client.AutoAddPolicy)
        with suppress(paramiko.ssh_exception.AuthenticationException):
            print("Connecting to {}".format(self.wan_intf_addr))
            ssh.connect(self.wan_intf_addr, username="root", password="")
        ssh_transport = ssh.get_transport()
        ssh_transport.auth_none("root")
        scp = SCPClient(ssh_transport)
        scp.put(local_image_path, remote_image_path)
        if (with_sha256sum):
            scp.put(local_image_path+".sha256sum", remote_image_path+".sha256sum")
        scp.close()

    def do_ssh_image_transfer(self,local_image_path, image_name):
        for command in ssh_firewall_commands:
            self.send_simple_command(command)
            time.sleep(0.5)

            self.send_simple_command("/etc/init.d/firewall reload")

            self.send_simple_command("/etc/init.d/dropbear restart")

            # Wait for dropbear to catch up
            time.sleep(5)

        test_image_local = "{}/{}".format(local_image_path,image_name)
        print("Local image path: {}".format(test_image_local))
        self.do_ssh_transfer(test_image_local,"/tmp/{}".format(image_name))

        self.client.expect(self.recovery_ps1,timeout=600)
        self.client.sendline(u'sha256sum -c {}.sha256sum'.format(image_name))
        self.client.expect('{}: OK'.format(image_name))
        self.client.expect(self.recovery_ps1)

        self.send_simple_command('blkdiscard {}'.format(self.remote_block_device), timeout=180)

        self.write_image(image_name)

        self.do_returncode_check(self.recovery_ps1)

    def write_image(self, filename):
        self.client.sendline(u'write-image-disk {} {}'.format(filename, self.remote_block_device))
        self.client.expect('{} written'.format(self.remote_block_device), timeout=900)

    def handle_img_gz(self, filename):
        # 4194304+0 records out
        records_written_expression = re.compile("\\d+\\+0 records out")

        self.client.sendline(u'dd if={} | gunzip -c | dd of={}'.format(filename,self.remote_block_device))
        self.client.expect(records_written_expression,timeout=600)

    def handle_first_root_login(self, oldpassword, newpassword):
        # Wait for the crap (e.g random: init to go away first)
        time.sleep(10)
        self.client.sendline(u'root')
        self.client.expect('Password: ')

        self.client.sendline(u'changethis')
        must_change = self.client.expect(["You are required to change your password immediately \(administrator enforced\)","Login incorrect"], timeout=10)
        if (must_change == 0):
            self.client.expect(u"Current password: ")
            self.client.sendline(oldpassword)
            self.client.expect(u"New password: ")
            self.client.sendline(newpassword)
            self.client.expect(u"Retype new password:")
            self.client.sendline(newpassword)
        else:
            self.client.expect(self.target_login)
            self.client.sendline("root")
            self.client.expect("Password: ")
            self.client.sendline(newpassword)
        self.client.expect(self.target_ps1)

    def set_target_ps1(self, newps1):
        self.target_ps1 = newps1

    def set_target_login_prompt(self, loginprompt):
        self.target_login = loginprompt

    def check_free_diskspace(self):
        self.client.sendline(u'df -h / | tail -n 1 | awk "{{print $5}}"')
        self.client.expect(percent_free_expression)
        use_percent = int(self.client.match.groups()[0])
        print("Disk space usage: {}%".format(use_percent))
        if (use_percent > 10):
            print("ERROR: Disk has {}% utilization (expect <%10), resize script did not work".format(use_percent))
            exit(1)

    def check_number_of_interfaces(self):
        # Check that all Ethernet interfaces enumerated
        self.client.sendline(u'')
        self.client.expect(self.target_ps1)
        self.client.sendline(u'echo -n "Number of ethernet interfaces: "; ls -la /sys/class/net | grep eth | wc -l; echo')
        time.sleep(1)
        self.client.expect(r'Number of ethernet interfaces: ([0-9]+)')
        num_of_eth_interfaces = int(self.client.match.groups()[0])
        if (num_of_eth_interfaces < self.expected_ethernet_intf):
            print(self.client.before)
            print("ERROR: Only {} Ethernet interfaces found, expecting {}".format(num_of_eth_interfaces,self.expected_ethernet_intf))
            exit(1)
        self.client.sendline(u'')
        self.client.expect(self.target_ps1)

    def has_default_route(self):
        self.client.sendline("route -n | grep '0.0.0.0'")
        self.do_returncode_check(self.target_ps1)

    def has_hardware_rtc(self):
        # Check that hwclock (hardware RTC) works
        self.send_simple_command('hwclock', self.target_ps1)
        self.do_returncode_check(self.target_ps1)

    def do_apt_get_update(self):
        # Check that the system can update packages
        self.send_simple_command('apt-get update',self.target_ps1)
        self.do_returncode_check(self.target_ps1)

    def do_zypper_update(self):
        self.send_simple_command('zypper update',self.target_ps1)
        self.do_returncode_check(self.target_ps1)

    def check_can_reset(self):
        # Check that system can reboot itself
        self.client.sendline(u'reboot')
        self.client.expect('=>',timeout=300)

    def set_stty_cols(self):
        self.client.sendline(u'stty cols 160')
        self.do_returncode_check(self.target_ps1)
