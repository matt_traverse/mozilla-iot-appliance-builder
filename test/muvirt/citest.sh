#!/bin/sh
set -e

LOGIN_HOSTNAME=${LOGIN_HOSTNAME:-debian}
FIRSTBOOT_WAIT_TIME=${FIRSTBOOT_WAIT_TIME:-120}
SCRATCH_DIR=$(uci get virt.system.scratch)
cd "${SCRATCH_DIR}"
mkdir "${PIPELINE_ID}"
cd "${PIPELINE_ID}"
mv "/tmp/${IMAGE_NAME}" .

echo "Destroying existing citest vm"
/etc/init.d/muvirt stop citest || :


uci delete virt.citest || :

muvirt-createvm "citest" 512 1 "${TEST_NETWORK}" "${SCRATCH_DIR}${PIPELINE_ID}/${IMAGE_NAME}"

/etc/init.d/muvirt start citest
sleep 1
socat -U - UNIX-CONNECT:/tmp/qemu-tty-citest > /tmp/cibootlog &
SOCAT_PID=$!
sleep ${FIRSTBOOT_WAIT_TIME}
HAS_ISSUE=0
echo "Checking VM reached login prompt"
grep "Linux version" /tmp/cibootlog  || :
(tail -n 20 /tmp/cibootlog | grep "${LOGIN_HOSTNAME} login:") || HAS_ISSUE=1
if [ "${HAS_ISSUE}" -eq 1 ]; then
  echo "WARNING: Did not finish boot"
  echo "Boot log as follows:"
  cat /tmp/cibootlog
fi
/etc/init.d/muvirt stop citest
kill "${SOCAT_PID}" || :
rm -rf "${SCRATCH_DIR}/${PIPELINE_ID}"

uci delete virt.citest || :

echo "Exiting with code ${HAS_ISSUE}"
exit "${HAS_ISSUE}"
