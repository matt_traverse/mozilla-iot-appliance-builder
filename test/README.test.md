The test system requires these environment variables:

## VM test
- `MUVIRT_HOST_KEY`: SSH host key for the muvirt instance
- `MUVIRT_USER_KEY`: SSH user key for the muvirt instance (currently only root user supported)
- `MUVIRT_TEST_HOST`: IP address/hostname of the muvirt instance
- `MUVIRT_TEST_NETWORK`: Network on the muvirt host to attach the VM to

The image will be downloaded to the system wide 'scratch' dir. The test process will
execute it in place (no volume will be created).

## Bare metal test
- `BMC_REDFISH_URL`: URL to the boards redfish instance (used to issue reset command)
- `REMOTE_CONSOLE_HOST` and `REMOTE_CONSOLE_PORT`: telnet (mTerm) server for the board
- `REMOTE_BLOCK_DEVICE`: Block device to write the image to
- `WAN_INTERFACE`: Ethernet interface to use to download the image
