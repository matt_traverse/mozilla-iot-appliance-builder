#!/bin/bash
set -e

cp -r mozilla-iot-gateway ../diskimage-builder/diskimage_builder/elements
if [ ! $(grep TRAVERSE_IOT_REPO_URL ../diskimage-builder/tox.ini) ]; then
  sed -i '/passenv=/a \\tTRAVERSE_IOT_REPO_URL' ../diskimage-builder/tox.ini
  sed -i '/passenv=/a \\tTRAVERSE_IOT_DIST' ../diskimage-builder/tox.ini
fi

export APPSTORE_ID="mozilla-iot-appliance"
export APPSTORE_NAME="Mozilla WebThings Gateway Appliance"
export APPSTORE_DESCRIPTION="WebThings Gateway preinstalled onto a Debian Buster instance. Root password is \"changethis\""

EXTRA_ELEMENTS="mozilla-iot-gateway" IMAGE_BASE_NAME="webthings" ../debian/build.sh
