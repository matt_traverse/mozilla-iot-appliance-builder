export TRAVERSE_IOT_DIST=${TRAVERSE_IOT_DIST:-${DIB_RELEASE}}
export TRAVERSE_IOT_REPO_URL=${TRAVERSE_IOT_REPO_URL:-https://archive.traverse.com.au/pub/ports/mozilla-iot/debian-repo/}
export TRAVERSE_MOZIOT_SOURCES_CONF="deb [trusted=yes] ${TRAVERSE_IOT_REPO_URL} ${TRAVERSE_IOT_DIST} main"
